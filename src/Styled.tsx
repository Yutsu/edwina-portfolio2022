import { createGlobalStyle } from "styled-components"
import { COLOR } from "./constants/Theme"

export const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${COLOR.primary.main};
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }
  h1,
  h2 {
    color: ${COLOR.common.white};
  }
`
