import { initialState, PopupSlice } from "./Slice"
import { PopupState } from "./Type"

export { initialState, PopupSlice }
export type { PopupState }

export default PopupSlice.reducer