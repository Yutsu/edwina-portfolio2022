import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { PopupState, PopupProps } from "./Type";

export const initialState: PopupState = {
  popupProps: {
    open: false,
    content: '',
    icon: null,
  },
}

export const PopupSlice = createSlice({
  name: 'popup',
  initialState,
  reducers: {
    setAlert: (state, action: PayloadAction<PopupProps>) => {
      state.popupProps = action.payload
    }
  }
})