export interface PopupState {
  popupProps: PopupProps
}

export interface PopupProps {
  open: false
  content: ""
  icon: null
}

export interface PopupRef {
  setAlert: (value: PopupState) => void
}
