import { Home } from "./containers/Home"
import { GlobalStyle } from "./Styled"

export const App = () => {
  return (
    <>
      <GlobalStyle />
      <Home />
    </>
  )
}
