import styled from "styled-components"
import { COLOR } from "../../constants/Theme"

export const Title = styled.h2`
  background-color: ${COLOR.primary.main};
`
